<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
<title>HELLO | JSP CALLED</title>
</head>
<body>
 JSP CALLED FROM CONTROLLER
 <p class=first>Hello ${nom}</p>
</body>
</html>