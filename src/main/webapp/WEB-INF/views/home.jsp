<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>
<h1 class=first>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>
<c:import url="/resources/menu.jsp" />
</body>
</html>
