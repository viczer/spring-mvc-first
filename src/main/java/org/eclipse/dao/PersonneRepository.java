package org.eclipse.dao;

import org.eclipse.models.Personne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonneRepository extends JpaRepository <Personne, Long> {
	
	

}
